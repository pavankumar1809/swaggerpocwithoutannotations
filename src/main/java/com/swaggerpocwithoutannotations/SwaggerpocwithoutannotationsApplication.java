package com.swaggerpocwithoutannotations;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class SwaggerpocwithoutannotationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwaggerpocwithoutannotationsApplication.class, args);
	}

	/*
	 * You can add the following(productApi()method and metaData())in the
	 * Configuration class also.
	 * 
	 * com.example.swaggerpoc.controller is the package of RestControllers.
	 * 
	 * api is the root path.
	 * 
	 * you can access swagger generates UI at
	 * http://localhost:8080/swagger-ui.html#/
	 * 
	 * you can access swagger generates josn at http://localhost:8080/v2/api-docs
	 */
	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.swaggerpocwithoutannotations.controller")).paths(paths())
				.build().apiInfo(metaData());
	}

	private Predicate<String> paths() {
		return or(regex("/v1/api.*"));
		// return or(regex("/v2/api/product.*"), regex("/v1/api.*"));
		// regex("/contacts.*"),
		// regex("/pet.*"),
		// regex("/test.*"));
	}

	private ApiInfo metaData() {
		return new ApiInfoBuilder().title("API DOCUMENTATION WITH SWAGGER")
				.description("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum "
						+ "has been the industry's standard dummy text ever since the 1500s, when an unknown printer "
						+ "took a "
						+ "galley of type and scrambled it to make a type specimen book. It has survived not only five "
						+ "centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
						+ "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum "
						+ "passages, and more recently with desktop publishing software like Aldus PageMaker including "
						+ "versions of Lorem Ipsum.")
				.termsOfServiceUrl("http://springfox.io").contact("Pavan").license("Apache License Version 2.0")
				.licenseUrl("https://github.com/springfox/springfox/blob/master/LICENSE").version("2.0").build();
	}

}
