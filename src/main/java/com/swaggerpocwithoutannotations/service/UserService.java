package com.swaggerpocwithoutannotations.service;

import java.util.List;

import com.swaggerpocwithoutannotations.domain.User;

public interface UserService {

	User findById(long id);

	User findByName(String name);

	void saveUser(User user);

	void updateUser(User user);

	List<User> findAllUsers();

	boolean isUserExist(User user);

}
